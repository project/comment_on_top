<?php declare(strict_types = 1);

namespace Drupal\comment_on_top\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns responses for Comment on top routes.
 */
final class CommentOnTopController extends ControllerBase {

  /**
   * Change all comments in this node from "1" to null in field stick on top.
   */
  private function changeAllLastCommentsToNull($node_id) {
    $comments = $this->entityTypeManager()->getStorage('comment')->loadByProperties([
      'entity_id' => $node_id,
      'entity_type' => 'node',
    ]);

    foreach ($comments as $comment) {

      if ($comment->field_stick_comment_on_top_boole->value === '1') {
        $comment->field_stick_comment_on_top_boole->setValue(null);
        $comment->save();
      }
    }
  }

  /**
   * Grant comment in this node from null to "1" in field stick on top.
   */
  public function stickOnTop($node_id, $comment_id) {
    $this->changeAllLastCommentsToNull($node_id);

    $comment = $this->entityTypeManager->getStorage("comment")->load($comment_id);
    $comment->field_stick_comment_on_top_boole->value = '1';
    $comment->save();

    $this->messenger()->addStatus($this->t('Comment is successfully sticked on top!'));
    $url = Url::fromRoute('entity.node.canonical', ['node' => $node_id]);
    return new RedirectResponse($url->toString());
  }

  /**
   * Remove comment in this node from "1" to null in field stick on top.
   */
  public function removeFromTop($node_id, $comment_id) {
    $comment = $this->entityTypeManager()->getStorage("comment")->load($comment_id);
    $comment->field_stick_comment_on_top_boole->value = null;
    $comment->save();

    $this->messenger()->addStatus($this->t('Comment is removed from top!'));
    $url = Url::fromRoute('entity.node.canonical', ['node' => $node_id]);
    return new RedirectResponse($url->toString());
  }
}
